C'est une page de connection et d'inscription, avec les fonctionnalités suivantes :
 - les mots de passe sont hashé en base de donnée (via Bcrypt)
 - pour éviter les injections SQL j'ai utilisé des requètes préparées grâce à PDO
 - de plus j'ai délégué le plus de traitement possible à la base de donnée