<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inscription</title>
    </head>
    <body>
        <div>Inscription</div>
        <form method="post" action="inscription.php">

            <label for="pseudo">Pseudo:</label>
            <input type="text" placeholder="Pseudo" name="pseudo" id="pseudo" required>
            <label for="mdp">Mot de passe:</label>
            <input type="password" placeholder="Mot de passe" name="pwd" id="pwd" required>
            <label for="confmdp">Confirmation mot de passe:</label>
            <input type="password" placeholder="Confirmer le mot de passe" name="pwdconf" id="pwdconf" required>
            <button type="submit" name="inscription">Envoyer</button>
        </form>
    </body>
</html>
